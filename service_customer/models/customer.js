const mongoose = require('mongoose');
const customerSchema = require('../schemas/customer');

customerSchema.statics = {
	create: function (data, cb) {
		let customer = new this(data);
		customer.save(cb);
	},

	get: function (query, cb) {
		this.find(query, cb);
	},

	getByName: function (query, cb) {
		this.find(query, cb);
	},

	update: function (query, updateData, cb) {
		this.findOneAndUpdate(query, { $set: updateData }, { new: true }, cb);
	},

	updateOrderNo: function (query, cb) {
		this.findOneAndUpdate(query, { $inc: { 'noOfOrders': 1 } }, { new: true }, cb);
	},

	delete: function (query, cb) {
		this.findOneAndDelete(query, cb);
	}
}

const customerModel = mongoose.model('Customer', customerSchema);
module.exports = customerModel;