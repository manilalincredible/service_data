var express = require('express'),
	indexRouter = require('./routes/index'),
	db = require('./database/db'),
	queueConsumer = require('./queue/consumer')
	app = express();

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

db();

var router = express.Router();
app.use('/api',router);
app.get('/', function (req, res, next) {
	res.send('Test1');
});
indexRouter(router);

queueConsumer();

var port = 3000;
app.set('port', port);
app.listen(port);

module.exports = app;