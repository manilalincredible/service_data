var Customers = require('../models/customer');
module.exports = function () {
    var q = 'orderUpdates';

    var open = require('amqplib').connect('amqp://radmin:radmin@54.224.226.168?heartbeat=60');

    open.then(function (conn) {
        return conn.createChannel();
    }).then(function (ch) {
        return ch.assertQueue(q).then(function (ok) {
            return ch.consume(q, function (msg) {
                if (msg !== null) {
                    let incomingMessage = JSON.parse(msg.content);
                    Customers.updateOrderNo({ _id: incomingMessage.customerId }, function (err, customer) {
                        if (!err) {
                            ch.ack(msg);
                        }
                    })
                }
            });
        });
    }).catch(console.warn);
}