# set the base image 
FROM node:12

# create a directory for the application code
RUN mkdir -p  /app/module_customer
WORKDIR /app/module_customer

#now install the application dependencies by first copying the package.json on local machine to the container and then run the npm install to install the dependencies
COPY package*.json /app/module_customer/
RUN npm install

#now copy the entire source code to the server
COPY . /app/module_customer

#now expose the port in which the code runs which is 3000
EXPOSE 3000

#now run the application by running the command npm start which will run the command node server.js
CMD [ "node", "app.js" ]