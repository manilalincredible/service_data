module.exports = function (data) {
    var q = 'customerUpdates';

    var open = require('amqplib').connect('amqp://radmin:radmin@54.224.226.168?heartbeat=60');

    // Publisher
    open.then(function (conn) {
        return conn.createChannel();
    }).then(function (ch) {
        return ch.assertQueue(q).then(function (ok) {
            return ch.sendToQueue(q, Buffer.from(JSON.stringify(data)));
        });
    }).catch(console.warn);
}