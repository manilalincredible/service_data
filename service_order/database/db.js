const mongoose = require('mongoose');
const dotenv = require('dotenv');
dotenv.config();

module.exports = function () {

	mongoose.connect('mongodb://3.87.134.66:27017/orders', { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false });

	process.on('SIGINT', function () {
		mongoose.connection.close(function () {
			console.log('Mongoose disconnected on app termination');
			process.exit(0)
		});
	});
}