var express = require('express'),
	indexRouter = require('./routes/index'),
	db = require('./database/db'),
	queueProcessor = require('./queue/receive')
app = express();

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

db();

var router = express.Router();
app.use('/api', router);
app.get('/', function (req, res, next) {
	res.send('OK');
});
indexRouter(router);

queueProcessor();

var port = 3001;
app.set('port', port);
app.listen(port);

module.exports = app;