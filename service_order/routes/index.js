var Orders = require('../controlers/order');

module.exports = function(router) {
    router.post('/order', Orders.createOrder);
    router.get('/order', Orders.getOrders);
    router.get('/order/:id', Orders.getOrder);
    router.put('/order/:id', Orders.updateOrder);
    router.delete('/order/:id', Orders.removeOrder);
}