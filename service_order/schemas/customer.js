var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var customerSchema = new Schema({
	customerId: {
		type: String,
		unique: true,
		required: true
	}
}, {
	timestamps: false
});

module.exports = customerSchema;