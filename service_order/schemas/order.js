var mongoose = require('mongoose');
var Customer = require('../models/customer');
var Schema = mongoose.Schema;
var orderSchema = new Schema({
	price: {
		type: Number,
		unique: false,
		required: true
	},
	customerId: {
		type: String,
		unique: false,
		required: true,
		validate: {
			validator: (value) => new Promise((resolve, reject) => {
				Customer.exists({ customerId: value }, function (err, isExists) {
					if(isExists) resolve(true); else resolve(false);
				});
			}),
			message: 'Customer id not found'
		}
	}
}, {
	timestamps: true
});

module.exports = orderSchema;