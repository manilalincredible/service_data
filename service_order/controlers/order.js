var Orders = require('../models/order');
var addTaskToQueue = require('../queue/sender');

exports.createOrder = function (req, res, next) {
    var order = {
        price: req.body.price,
        customerId: req.body.customerId
    };

    Orders.create(order, function (err, order) {
        if (err) {
            res.json({
                error: err
            })
        } else {
            addTaskToQueue({
                code: 1,
                customerId: req.body.customerId
            });
            res.json({
                message: "Order created successfully"
            })
        }
    })
}

exports.getOrders = function (req, res, next) {
    Orders.get({}, function (err, orders) {
        if (err) {
            res.json({
                error: err
            })
        }
        res.json({
            orders: orders
        })
    })
}

exports.getOrder = function (req, res, next) {
    Orders.get({ _id: req.params.id }, function (err, orders) {
        if (err) {
            res.json({
                error: err
            })
        }
        res.json({
            orders: orders
        })
    })
}

exports.updateOrder = function (req, res, next) {
    var order = {
        name: req.body.name,
        mobile: req.body.mobile
    };
    Orders.update({ _id: req.params.id }, order, function (err, order) {
        if (err) {
            res.json({
                error: err
            })
        }
        res.json({
            message: "Order updated successfully"
        })
    })
}

exports.removeOrder = function (req, res, next) {
    Orders.delete({ _id: req.params.id }, function (err, order) {
        if (err) {
            res.json({
                error: err
            })
        }
        res.json({
            message: "Order deleted successfully"
        })
    })
}