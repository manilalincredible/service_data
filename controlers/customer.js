var Customers = require('../models/customer');
var addTaskToQueue = require('../queue/sender');

exports.createCustomer = function (req, res, next) {
    var customer = {
        name: req.body.name,
        mobile: req.body.mobile
    };

    Customers.create(customer, function (err, customer) {
        if (err) {
            res.json({
                error: err
            })
        } else {
            addTaskToQueue({
                code: 1,
                customerId: customer.id
            });
            res.json({
                message: "Customer created successfully"
            })
        }
    })
}

exports.getCustomers = function (req, res, next) {
    Customers.get({}, function (err, customers) {
        if (err) {
            res.json({
                error: err
            })
        }
        res.json({
            customers: customers
        })
    })
}

exports.getCustomer = function (req, res, next) {
    Customers.get({ name: req.params.name }, function (err, customers) {
        if (err) {
            res.json({
                error: err
            })
        }
        res.json({
            customers: customers
        })
    })
}

exports.updateCustomer = function (req, res, next) {
    var customer = {
        name: req.body.name,
        mobile: req.body.mobile
    };
    Customers.update({ _id: req.params.id }, customer, function (err, customer) {
        if (err) {
            res.json({
                error: err
            })
        }
        res.json({
            message: "Customer updated successfully"
        })
    })
}

exports.removeCustomer = function (req, res, next) {
    Customers.delete({ _id: req.params.id }, function (err, customer) {
        if (err) {
            res.json({
                error: err
            })
        }
        res.json({
            message: "Customer deleted successfully"
        })
    })
}